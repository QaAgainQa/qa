from time import sleep

import allure
import pytest

from DriverActions.DriverActions import DriverActions
from Pages.GamePadPage.gamepad_page import GamePad
from altunityrunner import AltrunUnityDriver
from appium import webdriver

pytest_plugins = ["Tests.test_fixtures.qa_task_fixtures"]


class TestQaTask(DriverActions):
    @pytest.mark.usefixtures("set_up_drivers", "set_up_drivers")
    @allure.testcase(url=None, name='Выполнить прохождение Персонажем А')
    def test_finish_game_with_actor_a(self, game_menu, game_map):
        actor_a = GamePad('ActorA(Clone)', 0.12)

        with allure.step("Step 1: Нажимаем кнопку Play"):
            game_menu.wait_until_play_appears()
        with allure.step("Step 2: Уровень 1 корректно запустился"):
            assert game_map.get_current_map_name() == 'Map1(Clone)'
        with allure.step("Step 3: Проходим уровень 1"):
            actor_a.get_key_and_open_door('red')
            actor_a.go_to_exit()
        with allure.step("Step 4: Проверяем, есть ли окно Close Me!, если есть - закрываем"):
            game_map.check_close_me_window()
        with allure.step("Step 5: Нажимаем кнопку Next"):
            game_map.tap_next_window()
        with allure.step("Step 6: Уровень 2 корректно запустился"):
            assert game_map.get_current_map_name() == 'Map2(Clone)'
        with allure.step("Step 7: Проходим уровень 2"):
            actor_a.get_key_and_open_door('blue')
            actor_a.get_key_and_open_door('green')
            actor_a.get_key_and_open_door('red', up_flag=False)
            actor_a.go_to_exit()
        with allure.step("Step 8: Проверяем, есть ли окно Close Me!, если есть - закрываем"):
            game_map.check_close_me_window()
        with allure.step("Step 9: Нажимаем кнопку Next"):
            game_map.tap_next_window()
        with allure.step("Step 10: Уровень 3 корректно запустился"):
            assert game_map.get_current_map_name() == 'Map3(Clone)'
        with allure.step("Step 11: Проходим уровень 3"):
            actor_a.get_key_and_open_door('green')
            actor_a.get_key_and_open_door('red')
            actor_a.go_to_exit()
        with allure.step("Step 12: Проверяем, есть ли окно Close Me!, если есть - закрываем"):
            game_map.check_close_me_window()
        with allure.step("Step 13: Нажимаем кнопку Next"):
            game_map.tap_next_window()
        with allure.step("Step 14: Уровень 4 корректно запустился"):
            assert game_map.get_current_map_name() == 'Map4(Clone)'
        with allure.step("Step 15: Проходим уровень 4"):
            actor_a.go_to_exit()
        with allure.step("Step 16: Проверяем, есть ли окно Close Me!, если есть - закрываем"):
            game_map.check_close_me_window()
        with allure.step("Step 17: Нажимаем кнопку Next"):
            game_map.tap_next_window()
        with allure.step("Step 18: Уровень 5 корректно запустился"):
            assert game_map.get_current_map_name() == 'Map5(Clone)'
        with allure.step("Step 19: Проходим уровень 5"):
            actor_a.go_to_exit()
        with allure.step("Step 20: Проверяем, есть ли окно Close Me!, если есть - закрываем"):
            game_map.check_close_me_window()
        with allure.step("Step 21: Нажимаем кнопку Next"):
            game_map.tap_next_window()
        with allure.step("Step 22: Возвращаемся в меню, отображается кнопка Play"):
            assert game_menu.get_name_of_play_button() == 'Play'
        with allure.step("Step 23: Нажимаем кнопку Play"):
            game_menu.wait_until_play_appears()
        with allure.step(
                "Step 24: Прогресс сохранился после прохождения игры - текущий персонаж - ActorA, уровень - 5"):
            assert game_map.get_current_map_name() == 'Map5(Clone)'
            assert game_menu.get_name_of_current_actor() == 'ActorA(Clone)'

    @pytest.mark.usefixtures("set_up_drivers", "set_up_drivers")
    @allure.testcase(url=None, name='Выполнить прохождение Персонажем B')
    def test_finish_game_with_actor_b(self, game_menu, game_map):
        actor_b = GamePad('ActorB(Clone)', 0.10)
        with allure.step("Step 1: Выбираем персонажа B"):
            game_menu.select_player_2()
        with allure.step("Step 2: Проверяем выбранный персонаж - ActorB"):
            assert game_menu.check_current_actor() == 'ActorB'
        with allure.step("Step 3: Нажимаем кнопку Play"):
            game_menu.wait_until_play_appears()
        with allure.step("Step 4: Уровень 1 корректно запустился"):
            assert game_map.get_current_map_name() == 'Map1(Clone)'
        with allure.step("Step 5: Проходим уровень 1"):
            actor_b.get_key_and_open_door('red')
            actor_b.go_to_exit()
        with allure.step("Step 6: Проверяем, есть ли окно Close Me!, если есть - закрываем"):
            game_map.check_close_me_window()
        with allure.step("Step 7: Нажимаем кнопку Next"):
            game_map.tap_next_window()
        with allure.step("Step 8: Уровень 2 корректно запустился"):
            assert game_map.get_current_map_name() == 'Map2(Clone)'
        with allure.step("Step 9: Проходим уровень 2"):
            actor_b.get_key_and_open_door('blue')
            actor_b.get_key_and_open_door('green')
            actor_b.get_key_and_open_door('red', up_flag=False)
            actor_b.go_to_exit()
        with allure.step("Step 10: Проверяем, есть ли окно Close Me!, если есть - закрываем"):
            game_map.check_close_me_window()
        with allure.step("Step 11: Нажимаем кнопку Next"):
            game_map.tap_next_window()
        with allure.step("Step 12: Уровень 3 корректно запустился"):
            assert game_map.get_current_map_name() == 'Map3(Clone)'
        with allure.step("Step 13: Проходим уровень 3"):
            actor_b.get_key_and_open_door('green')
            actor_b.get_key_and_open_door('red')
            actor_b.go_to_exit()
        with allure.step("Step 14: Проверяем, есть ли окно Close Me!, если есть - закрываем"):
            game_map.check_close_me_window()
        with allure.step("Step 15: Нажимаем кнопку Next"):
            game_map.tap_next_window()
        with allure.step("Step 16: Уровень 4 корректно запустился"):
            assert game_map.get_current_map_name() == 'Map4(Clone)'
        with allure.step("Step 17: Проходим уровень 4"):
            actor_b.go_to_exit()
        with allure.step("Step 18: Проверяем, есть ли окно Close Me!, если есть - закрываем"):
            game_map.check_close_me_window()
        with allure.step("Step 19: Нажимаем кнопку Next"):
            game_map.tap_next_window()
        with allure.step("Step 20: Уровень 5 корректно запустился"):
            assert game_map.get_current_map_name() == 'Map5(Clone)'
        with allure.step("Step 21: Проходим уровень 5"):
            actor_b.go_to_exit()
        with allure.step("Step 22: Проверяем, есть ли окно Close Me!, если есть - закрываем"):
            game_map.check_close_me_window()
        with allure.step("Step 23: Нажимаем кнопку Next"):
            game_map.tap_next_window()
        with allure.step("Step 24: Возвращаемся в меню, отображается кнопка Play"):
            assert game_menu.get_name_of_play_button() == 'Play'
        with allure.step("Step 25: Нажимаем кнопку Play"):
            game_menu.wait_until_play_appears()
        with allure.step(
                "Step 26: Прогресс сохранился после прохождения игры - текущий персонаж - ActorA, уровень - 5"):
            assert game_map.get_current_map_name() == 'Map5(Clone)'
            assert game_menu.get_name_of_current_actor() == 'ActorB(Clone)'

    @pytest.mark.usefixtures("set_up_drivers")
    @allure.testcase(url=None, name='Выполнить прохождение Персонажем А')
    def test_save_progress_on_app_restart(self, game_menu, game_map):
        actor_a = GamePad('ActorA(Clone)', 0.12)

        with allure.step("Step 1: Нажимаем кнопку Play"):
            game_menu.wait_until_play_appears()
        with allure.step("Step 2: Уровень 1 корректно запустился"):
            assert game_map.get_current_map_name() == 'Map1(Clone)'
        with allure.step("Step 3: Проходим уровень 1"):
            actor_a.get_key_and_open_door('red')
            actor_a.go_to_exit()
        with allure.step("Step 4: Проверяем, есть ли окно Close Me!, если есть - закрываем"):
            game_map.check_close_me_window()
        with allure.step("Step 5: Нажимаем кнопку Next"):
            game_map.tap_next_window()
        with allure.step("Step 6: Уровень 2 корректно запустился"):
            assert game_map.get_current_map_name() == 'Map2(Clone)'

        with allure.step("Step 7: Перезапускаем приложение"):
            # По неизвестной причине выполнение рестарта приложения через одну сессию не работает
            # self.unity_driver.appium_driver().close_app()
            # self.unity_driver.appium_driver().launch_app()

            # Поэтому создаем еще одну
            CAPS = {
                'platformName': 'Android',
                'appPackage': 'com.Nekki.QATestTask',
                'deviceName': 'auto',
                'appActivity': 'com.unity3d.player.UnityPlayerActivity',
                'noReset': True,
                'newCommandTimeout': 60

            }
            appium = webdriver.Remote('http://localhost:4723/wd/hub', CAPS)
            sleep(10)

            unity = AltrunUnityDriver(appium, 'android', TCP_IP='localhost')
            new_session = DriverActions()
            new_session.init_appium_driver(appium)
            new_session.init_unity_driver(unity)

        with allure.step("Step 8: Нажимаем кнопку Play"):
            game_menu.wait_until_play_appears()
        with allure.step("Step 9: Прогресс корректно сохранился - Отображается Уровень 2"):
            assert game_map.get_current_map_name() == 'Map2(Clone)'

            new_session.unity_driver.stop()
            new_session.appium_driver.quit()
