Тесты запускаются с помощью pytest
Запустить тесты из коммандной строки  - pytest path/to/test_qa_task.py
Коректировать параметры appium, altunityrunner  можно в conftest.py

Для корректного запуска тестов необходимо установить генератор отчетов allure:
https://docs.qameta.io/allure/

Для выгрузки отчетов необходимо добавить дополнительный аргумент в строку --allredir=path/to/dir, если не казан, то
будет автоматически создан в Tests/allure-report

Посмотреть отчет можно выполнив - allure serve path/to/dir

Финальная строка может иметь вид - pytest path/to/test_qa_task.py --alluredir --allredir=path/to/report