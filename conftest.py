import os
import shutil

import pytest
from altunityrunner import AltrunUnityDriver
from appium import webdriver
from time import sleep

from DriverActions.DriverActions import DriverActions

ALLURE_REPORT = '../Tests/allure-report'

CAPS = {
    'platformName': 'Android',
    'appPackage': 'com.Nekki.QATestTask',
    'deviceName': 'auto',
    'appActivity': 'com.unity3d.player.UnityPlayerActivity',
    'noReset': False,
    'newCommandTimeout': 60

}

@pytest.fixture
def set_up_drivers():
    appium_driver = webdriver.Remote('http://localhost:4723/wd/hub', CAPS)
    DriverActions().init_appium_driver(appium_driver)
    # Без слипа, перед полной загрузкой приложения до меню, altunityrunner не отвечает
    sleep(10)
    unity_driver = AltrunUnityDriver(appium_driver, 'android', TCP_IP='localhost')
    DriverActions().init_unity_driver(unity_driver)

@pytest.fixture
def tear_down_drivers():
    yield
    DriverActions().unity_driver.stop()
    DriverActions().appium_driver.quit()



def pytest_configure(config):
    """
    Если путь для отчета allure не был задан - устанавливаем путь ../Tests/allure-report

    :param config:
    :return:
    """

    # Удаляем директорию с отчетом если существует, перед тестами
    if os.path.exists(ALLURE_REPORT):
        shutil.rmtree(ALLURE_REPORT)

    if config.option.allure_report_dir is None:
        config.option.allure_report_dir = ALLURE_REPORT
