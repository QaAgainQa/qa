import pytest

from DriverActions.DriverActions import DriverActions
from Pages.MapPage.map_page import MapPage
from Pages.MenuPage.menu_page import MenuPage


@pytest.fixture
def game_map():
    return MapPage()

@pytest.fixture
def game_menu():
    return MenuPage()

@pytest.fixture
def reset_app():
    DriverActions().appium_driver.reset()


