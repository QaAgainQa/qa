import json

from DriverActions.DriverActions import DriverActions


class MenuPage(DriverActions):

    def select_player_2(self):
        self.tap_element('Settings')
        self.tap_element('Next')
        self.tap_element('Back')

    def check_current_actor(self):
        data = self.get_sprite_name_of_element(self.unity_driver.find_element('Actor'))
        #Ответ был некорректен, поэтому немного поправил
        current_actor = json.loads(data.replace("'b'", ''))
        return current_actor['name']

    def get_name_of_current_actor(self):
        return self.unity_driver.find_element_where_name_contains('Actor').name

    def wait_until_play_appears(self):
        self.unity_driver.wait_for_element("Play").tap()

    def wait_for_main_scene(self):
        self.unity_driver.wait_for_current_scene_to_be("main").tap()

    def get_name_of_play_button(self):
        return self.unity_driver.find_element('Play').name
