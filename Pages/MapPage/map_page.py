from DriverActions.DriverActions import DriverActions


class MapPage(DriverActions):
    colors = {
        'RGBA(1.000, 0.000, 0.000, 1.000)': 'red',
        'RGBA(0.000, 1.000, 0.000, 1.000)': 'green',
        'RGBA(0.000, 0.000, 1.000, 1.000)': 'blue',
    }

    def get_all_doors(self):
        return self.unity_driver.find_elements('Door')

    def get_colored_doors(self):
        doors = {self.colors[self.get_sprite_color_of_element(door)]:door for door in self.get_all_doors()}
        return doors

    def get_all_keys(self):
        return self.unity_driver.find_elements('Key')

    def get_colored_keys(self):
        keys = {self.colors[self.get_sprite_color_of_element(key)]:key for key in self.get_all_keys()}
        return keys

    def check_close_me_window(self):
        try:
            self.unity_driver.find_element('Close')
            self.tap_element('Close')
        except Exception as e:
            print(repr(e))

    def tap_next_window(self):
        self.unity_driver.wait_for_element('Next')
        self.tap_element('Next')

    def get_current_map_name(self):
        return self.unity_driver.find_element_where_name_contains('Map').name