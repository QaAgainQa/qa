from DriverActions.DriverActions import DriverActions
from Pages.MapPage.map_page import MapPage


class GamePad(DriverActions):

    def __init__(self, actor, actor_speed):
        self.actor = actor
        self.actor_speed = actor_speed

    up = 'Up'
    left = 'Left'
    right = 'Right'
    interact = 'Interact'

    last_position = None

    def go_right_to_target(self, target,  low_edge, high_edge):
        current_position = int(float(self.unity_driver.find_element(self.actor).worldX) * -1)
        target_position = int(float(target.worldX) * -1)
        if self.last_position != current_position and current_position not in range(target_position - low_edge, target_position + high_edge):
            self.tap_element(self.right, self.actor_speed)
            self.last_position = current_position
            self.go_right_to_target(target,  low_edge, high_edge)

    def go_left_to_target(self, target,  low_edge, high_edge):
        current_position = int(float(self.unity_driver.find_element(self.actor).worldX) * -1)
        target_position = int(float(target.worldX) * -1)
        if self.last_position != current_position and current_position not in range(target_position - low_edge, target_position + high_edge):
            self.tap_element(self.left, self.actor_speed)
            self.last_position = current_position
            self.go_left_to_target(target,  low_edge, high_edge)

    def go_up_to_target(self, target,   low_edge=2):
        current_position = int(float(self.unity_driver.find_element(self.actor).worldY) * -1)
        target_position = int(float(target.worldY) * -1)
        if current_position > -8 and current_position not in range(target_position - low_edge, target_position):
            self.tap_element(self.up, self.actor_speed + 0.08)
            self.go_up_to_target(target,  low_edge)

    def go_to_object(self, target, up_flag=False, right_low_edge=2, right_high_edge=2, left_low_edge=2,
                     left_high_edge=3):
        current_position = self.unity_driver.find_element(self.actor)
        target_position = target

        if float(target_position.worldX) > float(current_position.worldX):
            self.go_right_to_target(target_position,  right_low_edge, right_high_edge)
        else:
            self.go_left_to_target(target_position,  left_low_edge, left_high_edge)
        if up_flag is True:
            self.go_up_to_target(target_position)

    def go_to_door(self, target):
        self.go_to_object(target, up_flag=False, right_high_edge=7, left_low_edge=7)

    def go_to_key(self, target, up_flag=False):
        self.go_to_object(target, up_flag=up_flag)

    def get_key_and_open_door(self, color, up_flag=True):
        doors = MapPage().get_colored_doors()
        keys = MapPage().get_colored_keys()
        self.go_to_key(keys[color], up_flag)
        self.interact_object()
        self.go_to_door(doors[color])
        self.interact_object()

    def go_to_exit(self):
        exit = self.unity_driver.find_element('Exit')
        self.go_to_object(exit, right_high_edge=4, left_low_edge=4)

    def interact_object(self):
        self.unity_driver.find_element('Interact').mobile_tap(0.25)
