class DriverActions():
    appium_driver = None
    unity_driver = None

    @classmethod
    def init_appium_driver(cls, appium_driver):
        cls.appium_driver = appium_driver

    @classmethod
    def init_unity_driver(cls, unity_driver):
        cls.unity_driver = unity_driver

    def tap_element(self, element, duration=0.3):
        self.unity_driver.find_element(element).mobile_tap(duration)

    def get_sprite_color_of_element(self, element):
        return element.get_component_property("UnityEngine.SpriteRenderer", 'color')

    def get_sprite_name_of_element(self, element):
        return element.get_component_property("UnityEngine.UI.Image", 'sprite')


